# Docker file - an blueprint to create docker image,

FROM openjdk:21 

RUN mkdir /usr/app/ 

COPY target/Spring_Batch_App.jar /usr/app/

WORKDIR /usr/app/

ENTRYPOINT [ "java","-jar","Spring_Batch_App.jar" ]