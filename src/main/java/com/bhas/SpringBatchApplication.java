package com.bhas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBatchApplication
{

	public static void main(String[] args) 
	{
		SpringApplication.run(SpringBatchApplication.class, args);
		System.out.println("Spring Batch is an open-source framework for batch processing and execution of a series of jobs.");
		System.out.println("naveen");
		System.out.println("used to build rest apis");
	}

}
