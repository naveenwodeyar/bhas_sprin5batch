package com.bhas.config;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.job.builder.JobBuilder;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.step.builder.StepBuilder;
import org.springframework.batch.item.data.RepositoryItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.transaction.PlatformTransactionManager;

import com.bhas.modal.Employee;
import com.bhas.repo.EmployeeRepo;

@Configuration
@EnableBatchProcessing
public class EmployeeBatchConfig 
{
	// Constructor Dependency injection,
	@Autowired
	private EmployeeRepo employeeRepo;
	
	@Autowired
//	private StepBuilderFactory stepBuilderFactory;
	private StepBuilder stepBuilder;	
	
	@Autowired
//	private JobBuilderFactory jobBuilder;
	private JobBuilder joBuilder;
	
	
	// 1.create Reader, to read the data.
	@Bean
	public FlatFileItemReader<Employee> fileReader()
	{
		// create the FlatFileItemReader object,
		FlatFileItemReader<Employee> itemReader = new FlatFileItemReader<>();
		// provide the file address,			
		itemReader.setResource(new FileSystemResource("src/main/resources/Employee.csv")); 
		
		itemReader.setName("File-Reader");			// set an custom name for the FlatFileItemReade,
		itemReader.setLinesToSkip(1);				// skip the header or first row,
		itemReader.setLineMapper(lineMapper());
		
		return itemReader;
	}

	// convert the data into the java object,
	private LineMapper<Employee> lineMapper()
	{
		DefaultLineMapper<Employee> lineMapper = new DefaultLineMapper<>();
		
		DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
		lineTokenizer.setDelimiter(" ");
		lineTokenizer.setStrict(false);
		lineTokenizer.setNames("eId","First_Name","Lirst_Name","Emp_Email","Emp_Gender","Emp_IP_Address","Emp_Salary","Emp_Country");

		BeanWrapperFieldSetMapper<Employee> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
		fieldSetMapper.setTargetType(Employee.class);
		
		lineMapper.setLineTokenizer(lineTokenizer);
		lineMapper.setFieldSetMapper(fieldSetMapper);;
		return null;
	}
	
	// 2. Create the processor,
	@Bean
	public EmployeeProcessor employeProcessor()
	{
		return new EmployeeProcessor();
	}
	
	@Bean
	// 3. Create the ItemWrite to write into the destination,
	public RepositoryItemWriter<Employee> customeWriter()
	{
		RepositoryItemWriter<Employee> repositoryWriter = new RepositoryItemWriter<>();
		repositoryWriter.setRepository(employeeRepo);
		repositoryWriter.setMethodName("save");
	
		return repositoryWriter;
	}
	
	// 4. create step,
//	@Bean
//	public Step step()
//	{
//		return stepBuilder.get("step-1").<Employee,Employee>chunk(20)
//								 .reader(fileReader())
//								 .processor(employeProcessor())
//								 .writer(customeWriter())
//								 .build();
//	}
	
	@Bean
	public Step sampleStep(JobRepository jobRepository,PlatformTransactionManager platformTransactionManager)
	{
		return new StepBuilder("sampleStep",jobRepository)
					.<Employee,Employee>chunk(20,platformTransactionManager)
					.reader(fileReader())
					.writer(customeWriter())
					.build();
	}
	
	// 5. create the job
//	@Bean
//	public Job job()
//	{
//		return jobBuilder.get("Employee-Job")
//						 .flow(step())
//						 .end()
//						 .build();
//	}
	
	@Bean(name = "launcher")
	public Job sampleJob(JobRepository jobRepository,Step sampleStep)
	{
		return new JobBuilder("sampleJob",jobRepository)
					.start(sampleStep)
					.build();
	}
	
	
}
