package com.bhas.controller;

import org.springframework.batch.core.*;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/employee")
@Slf4j
public class EmployeeController
{
	// Dependency Injection.,
	@Autowired
	private JobLauncher jobLauncher;
	
	@Autowired
	private Job job;
	
	@GetMapping
	public String testMsg()
	{
		log.info("Logger's");
		return "Welcome to the Spring_Batch Application,";
	}
	
	@GetMapping("/insertData")
	@ResponseStatus(code = HttpStatus.FOUND)
	public void insertToDatabase() throws JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException, JobParametersInvalidException
	{
		JobParameters jobParameters = 
				new JobParametersBuilder().addLong("Start-At", System.currentTimeMillis()).toJobParameters();
				jobLauncher.run(job, jobParameters);
	}
}
